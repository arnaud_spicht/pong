// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "Ball.generated.h"

UCLASS()
class PONG_API ABall : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ABall();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void Tick( float DeltaSeconds ) override;
	
	/** Return the mesh for ball */
	FORCEINLINE class UStaticMeshComponent* GetMesh() const { return BallMesh; }

	/* init's velocity of the ball in the Launch direction */
	void InitVelocity(const FVector& LaunchDirection);
	

protected:
	/** Speed of the ball */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Ball")
	float InitialSpeed;

	/** Called when overlap other actor */
	UFUNCTION()
	void OnBeginOverlap(class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult & SweepResult);

	UFUNCTION(BlueprintNativeEvent, Category = "Ball")
	void OnHitObject(AActor* OtherActor);

	UFUNCTION(BlueprintNativeEvent, Category = "Ball")
	void OnHitGoal(AActor* OtherActor);

private:
	/** Static mesh to represent the ball in the level */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Ball", meta = (AllowPrivateAccess = "true"))
	class UStaticMeshComponent* BallMesh;
	
};
