// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Pawn.h"
#include "Paddle.generated.h"

UCLASS()
class PONG_API APaddle : public APawn
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	APaddle();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void Tick( float DeltaSeconds ) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* InputComponent) override;

	/** Return the mesh for paddle */
	FORCEINLINE class UStaticMeshComponent* GetMesh() const { return PaddleMesh; }

protected:
	/** Force rate */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Paddle")
	float ForceRate;

	/** Called for up/down input */
	void MoveUp(float Value);

	/** Called to pause game */
	void Pause();

	/** Static mesh to represent the paddle in the level */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Paddle", meta = (AllowPrivateAccess = "true"))
	class UStaticMeshComponent* PaddleMesh;
	
	
};
