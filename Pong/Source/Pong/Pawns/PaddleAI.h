// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Pawns/Paddle.h"
#include "PaddleAI.generated.h"

/**
 * 
 */
UCLASS()
class PONG_API APaddleAI : public APaddle
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	APaddleAI();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	// Called every frame
	virtual void Tick(float DeltaSeconds) override;
	
	// Ball Reference
	void SetBall(class ABall* Ball);

private:

	//Reference to Ball
	class ABall* Ball;

	float MeshHeight;
	
	
};
