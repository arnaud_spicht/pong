// Fill out your copyright notice in the Description page of Project Settings.

#include "Pong.h"
#include "PongGameState.h"
#include "ScoreComponent.h"


// Sets default values for this component's properties
UScoreComponent::UScoreComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	bWantsBeginPlay = true;
	PrimaryComponentTick.bCanEverTick = false;
	ForPlayer = EPlayerEnum::PE_1;

	// ...
}


// Called when the game starts
void UScoreComponent::BeginPlay()
{
	Super::BeginPlay();
	AActor* Owner = GetOwner();
	ScoreDisplay = Cast<UTextRenderComponent>(Owner->GetComponentByClass(UTextRenderComponent::StaticClass()));
	if (!ScoreDisplay)
	{
		return;
	}
	UWorld* TheWorld = GetWorld();
	APongGameState* GameState = TheWorld->GetGameState<APongGameState>();
	if (GameState) {
		UE_LOG(PongLog, Log, TEXT("UScoreComponent binding PlayerScored"));
		GameState->OnScoreChanged().AddUObject(this, &UScoreComponent::PlayerScored);
	}
	// ...
	
}


// Called every frame
void UScoreComponent::TickComponent( float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction )
{
	Super::TickComponent( DeltaTime, TickType, ThisTickFunction );

	// ...
}

void UScoreComponent::PlayerScored(EPlayerEnum Player, uint8 Score)
{
	UE_LOG(PongLog, Log, TEXT("UScoreComponent Player Scored %d"), Score);

	if (Player == ForPlayer)
	{
		if (ScoreDisplay)
		{
			ScoreDisplay->SetText(FText::AsNumber(Score));
		}
	}
	
}
