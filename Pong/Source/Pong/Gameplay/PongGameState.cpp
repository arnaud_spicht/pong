// Fill out your copyright notice in the Description page of Project Settings.

#include "Pong.h"
#include "PongGameState.h"
#include "PongGameMode.h"



void APongGameState::PlayerScored(EPlayerEnum Player)
{
	uint8 Score;
	
	if (Player == EPlayerEnum::PE_2) 
	{ 
		Score = ++Score_P2; 
	}
	else if(Player == EPlayerEnum::PE_1)
	{
		Score = ++Score_P1;
	}
	else
	{
		return;
	}
	UE_LOG(PongLog, Log, TEXT("APongGameState Player Scored"));
	ScoreChanged.Broadcast(Player, Score);
		
}

void APongGameState::ResetScore()
{
	Score_P1 = Score_P2 = 0;
	ScoreChanged.Broadcast(EPlayerEnum::PE_1, Score_P1);
	ScoreChanged.Broadcast(EPlayerEnum::PE_2, Score_P2);
}