// Fill out your copyright notice in the Description page of Project Settings.

#include "Pong.h"
#include "Pawns/PaddleAI.h"
#include "Actors/Ball.h"
#include "Pong1PGameMode.h"


APong1PGameMode::APong1PGameMode(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
}

void APong1PGameMode::BeginPlay()
{
	Super::BeginPlay();
	UWorld* const World = GetWorld();
	if (World)
	{
		APaddle* playerPaddle = (APaddle*)World->GetFirstPlayerController()->GetPawn();
		float playerPosY = playerPaddle->GetMesh()->GetComponentLocation().Y;
		FActorSpawnParameters SpawnParams;
		SpawnParams.Owner = this;
		SpawnParams.Instigator = Instigator;
		const FVector Center(0, -playerPosY, 0);
		const FRotator Direction(0, 0, 0);
		APaddleAI* PaddleAI = (APaddleAI*)World->SpawnActor<APaddleAI>(APaddleAI::StaticClass(), Center, Direction, SpawnParams);
		PaddleAI->SetBall(MyBall);
		FInputModeGameOnly InputMode;
		World->GetFirstPlayerController()->SetInputMode(InputMode);				
	}
}

void APong1PGameMode::GameOver()
{
	Super::GameOver();
	const FVector Zero(0, 0, 0);
	MyBall->InitVelocity(Zero);
	GetWorld()->GetTimerManager().SetTimer(GameOverTimerHandle, this, &APong1PGameMode::OpenMenuLevel, 3, false);
}

void APong1PGameMode::OpenMenuLevel()
{
	GetWorld()->GetTimerManager().ClearTimer(GameOverTimerHandle);
	UGameplayStatics::OpenLevel(GetWorld(), "Menu");
}