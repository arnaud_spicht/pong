// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/GameMode.h"
#include "PongGameMode.generated.h"

enum class EPlayerEnum : uint8;

/**
 * 
 */
UCLASS()
class PONG_API APongGameMode : public AGameMode
{
	GENERATED_BODY()

public:
	APongGameMode(const FObjectInitializer& ObjectInitializer);

	UPROPERTY(EditAnywhere, noclear, BlueprintReadOnly, Category = Classes)
	TSubclassOf<class ABall> BallClass;

	// Called when the game starts
	virtual void BeginPlay() override;

	/** called when the ball hit a goal */
	UFUNCTION()
	void PlayerScored(EPlayerEnum Player, uint8 Score);

	/** called when the we pause the game */
	UFUNCTION()
	void RequestPause();

protected:
	/** Score to win */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Pong")
	float ScoreToWin;

	ABall* MyBall;

	/** called when the game is over */
	UFUNCTION()
	virtual void GameOver();

	UFUNCTION(BlueprintNativeEvent, Category = "Pong")
	void OnPauseRequest();

private:
	UTextRenderComponent* ScoreDisplay_P1;

	UTextRenderComponent* ScoreDisplay_P2;
		
};
