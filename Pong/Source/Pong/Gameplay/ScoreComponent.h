// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Components/ActorComponent.h"
#include "ScoreComponent.generated.h"


UCLASS( ClassGroup=(Pong), meta=(BlueprintSpawnableComponent) )
class PONG_API UScoreComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UScoreComponent();

	// Called when the game starts
	virtual void BeginPlay() override;

	/** called when the ball hit a goal */
	UFUNCTION()
	void PlayerScored(EPlayerEnum Player, uint8 Score);
	
	// Called every frame
	virtual void TickComponent( float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction ) override;

protected:
	/** Player who scores */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Score")
	EPlayerEnum ForPlayer;

private:
	UTextRenderComponent* ScoreDisplay;
	
};
