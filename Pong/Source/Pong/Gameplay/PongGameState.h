// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/GameState.h"
#include "PongGameState.generated.h"

UENUM(BlueprintType)
enum class EPlayerEnum : uint8
{
	PE_1 UMETA(DisplayName = "Player 1"),
	PE_2 UMETA(DisplayName = "Player 2")
};


/**
 * 
 */
UCLASS()
class PONG_API APongGameState : public AGameState
{
	GENERATED_BODY()
public:
	/** Broadcast when a player scores */
	DECLARE_EVENT_TwoParams(APongGameState, FScoreChangedEvent, const EPlayerEnum, const uint8);
	FScoreChangedEvent& OnScoreChanged() { return ScoreChanged; }

	/** Return the score for Player */
	FORCEINLINE uint8 GetScore(EPlayerEnum Player) const {
		if (Player == EPlayerEnum::PE_2) { return Score_P2; }
		return Score_P1;
	}
	
	void PlayerScored(EPlayerEnum Player);

	UFUNCTION()
	void ResetScore();
	
protected:
	/** current score for Player 1 */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Score")
	uint8 Score_P1 = 0;

	/** current score for Player 2 */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Score")
	uint8 Score_P2 = 0;

private:
	/** Broadcasts whenever the score changes */
	FScoreChangedEvent ScoreChanged;
		
};
