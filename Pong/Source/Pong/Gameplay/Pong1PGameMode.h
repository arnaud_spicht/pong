// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "PongGameMode.h"
#include "Pong1PGameMode.generated.h"



/**
 * Pong 1 player Game Mode
 */
UCLASS()
class PONG_API APong1PGameMode : public APongGameMode
{
	GENERATED_BODY()
public:
	APong1PGameMode(const FObjectInitializer& ObjectInitializer);

	// Called when the game starts
	virtual void BeginPlay() override;

protected:
	/** called when the game is over */
	UFUNCTION()
	virtual void GameOver() override;

private:
	FTimerHandle GameOverTimerHandle;

	void OpenMenuLevel();
	
};
