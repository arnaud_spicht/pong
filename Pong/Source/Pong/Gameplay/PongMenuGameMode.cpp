// Fill out your copyright notice in the Description page of Project Settings.

#include "Pong.h"
#include "Pawns/PaddleAI.h"
#include "PongGameState.h"
#include "PongMenuGameMode.h"


APongMenuGameMode::APongMenuGameMode(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	DefaultPawnClass = APawn::StaticClass();
	ScoreToWin = 2;
}

void APongMenuGameMode::BeginPlay()
{
	Super::BeginPlay();
	UWorld* const World = GetWorld();
	if (World)
	{
		FActorSpawnParameters SpawnParams;
		SpawnParams.Owner = this;
		SpawnParams.Instigator = Instigator;
		const FRotator Direction(0, 0, 0);

		const AActor* StartSpot = FindPlayerStart(NULL);
		
		if (StartSpot && MyBall) {
			const FVector Paddle1Location = StartSpot->GetActorLocation();
			UE_LOG(PongLog, Log, TEXT("APongMenuGameMode spawning Paddle1..."));
			APaddleAI* Paddle1 = (APaddleAI*)World->SpawnActor<APaddleAI>(APaddleAI::StaticClass(), Paddle1Location, Direction, SpawnParams);						
			
			if (Paddle1) 
			{
				Paddle1->SetBall(MyBall);
			}
			else
			{
				UE_LOG(PongLog, Log, TEXT("APongMenuGameMode no Paddle1!"));
			}

			const FVector Paddle2Location(0, -Paddle1Location.Y, 0);

			APaddleAI* Paddle2 = (APaddleAI*)World->SpawnActor<APaddleAI>(APaddleAI::StaticClass(), Paddle2Location, Direction, SpawnParams);
			if (Paddle2)
			{
				Paddle2->SetBall(MyBall);
			}
			
		}
	}
}

void APongMenuGameMode::GameOver()
{
	UWorld* const World = GetWorld();
	if (World)
	{
		APongGameState* GameState = World->GetGameState<APongGameState>();
		if (GameState) {
			GameState->ResetScore();
		}
	}
}
